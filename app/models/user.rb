class User < ActiveRecord::Base
  include ActiveModel::Validations
  validates :password, presence: true
  validates :name, presence: true, uniqueness: true
  validates :email, presence: true
  validates_email_format_of :email, :message => 'is not looking good'

  # if email and password correspond to a valid user, return that user
  # otherwise, return nil
  def self.authenticate(email, password)
    @user = User.where(email: email).first
    if @user && @user.password == password
      return @user
    else
      return nil
    end
  end

  # private
  #   def validate_email
  #     errors.add(:email, "is not looking good") if ValidatesEmailFormatOf::validate_email_format(email)
  #   end

end
