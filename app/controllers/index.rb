use Rack::Session::Pool, :expire_after => 2592000

get '/' do
  # Look in app/views/index.erb
  erb :index
end

# create account
post '/new' do
  @user = User.new(name: params[:name], email: params[:email], password: params[:password])
  if @user.save
    erb :new
  else
    erb :index
  end
end

# logging in
post '/login' do
  @user = User.authenticate(params[:email], params[:password])
  if @user
    session[:user_id] = @user.id
    redirect to "/secret"
  else
    redirect to "/"
  end
end

get '/secret' do
  @user = User.find session[:user_id]
  erb :secret
end

get '/logout' do
  session.clear
  redirect '/'
end

